#!/bin/bash

# USAGE:
# bash job_conf_helper.sh mspurity_spectralmatching 4 8
# bash job_conf_helper.sh mspurity_spectralmatching 4 8 destinations
# bash job_conf_helper.sh mspurity_spectralmatching 4 8 tools

# for tool in mspurity_spectralmatching mspurity_flagremove mspurity_purityx; do bash job_conf_helper.sh $tool 4 8 destinations; done
# for tool in mspurity_spectralmatching mspurity_flagremove mspurity_purityx; do bash job_conf_helper.sh $tool 4 8 tools; done


TOOL=$1
CPU=$2
MEM=$3
MEM_RESUBMIT=$(expr $MEM \* 3 / 2)
WHAT=$4

if [ -z $WHAT ] || [ $WHAT = 'destinations' ]; then
echo "\
  - id: slurm-$TOOL-thread$(printf "%02d" $CPU)-mem$(printf "%03d" $MEM)
    params:
      nativeSpecification: --partition=fast  --cpus-per-task=$CPU --mem=$(expr $MEM \* 1000)
    resubmit:
      condition: memory_limit_reached
      destination: slurm-$TOOL-thread$(printf "%02d" $CPU)-mem$(printf "%03d" $MEM_RESUBMIT)_resubmit_mem
    runner: slurm
  - id: slurm-$TOOL-thread$(printf "%02d" $CPU)-mem$(printf "%03d" $MEM_RESUBMIT)_resubmit_mem
    params:
      nativeSpecification: --partition=fast  --cpus-per-task=$CPU --mem=$(expr $MEM_RESUBMIT \* 1000)
    runner: slurm"
fi

if [ -z $WHAT ] || [ $WHAT = 'tools' ]; then
echo "\
  - destination: slurm-$TOOL-thread$(printf "%02d" $CPU)-mem$(printf "%03d" $MEM)
    id: $TOOL"
fi
