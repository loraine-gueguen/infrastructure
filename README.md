# Description

This repository installs and configures the usegalaxy.fr instance of IFB

It will install:

- a nginx server on ubuntu with a configured proftpd service (using the geerlingguy.nginx and the galaxyproject.ansible-proftpd roles)
- a postgres server on centos (using the anxs.postgresql role)
- a galaxy server on centos (using the galaxyproject.galaxy and usegalaxy-eu.supervisor roles)

* Development instance: https://usegalaxy-dev.dev.ifb.local
* Preproduction instance: https://usegalaxy.ifb.local/
* Production instance: https://usegalaxy.fr


These playbooks assume the following steps have already been completed

* [x]  The ldap client, and ssh client were installed on the three servers
* [x]  An LDAP user "galaxy" exists
* [x]  The slurm client and moosefs client have been installed on the galaxy server
* [x]  DRMAA_LIBRARY_PATH var set to lib path to slurm-drmaa eg export DRMAA_LIBRARY_PATH=/shared/software/slurm-drmaa/1.0.8/libdrmaa.so.1.0.8
* [x]  The moose client was installed on the nginx server and galaxy directory was mounted  

## Monitoring

[GTN - Monitoring](https://galaxyproject.github.io/training-material/topics/admin/tutorials/monitoring/tutorial.html)

To monitore Galaxy instances in real time (load, CPU, disk usage, running jobs ...).

### Telegraf

[Telegraf](https://github.com/influxdata/telegraf)

Telegraf get data from Galaxy (thanks to [statsd](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/statsd) plugin ) and store it in the InfluxDB database.
Telegraf is installed with the ansible role 'dj-wasabi.telegraf' in playbook_galaxy-extra.yml

#### Gxadmin

[GTN - gxadmin](https://galaxyproject.github.io/training-material/topics/admin/tutorials/gxadmin/tutorial.html)

To get statistics about other Galaxy-specific metrics such as the job queue status, we need to use gxadmin to query the Galaxy database and configure Telegraf to consume this data.
Gxadmin is installed with the ansible role 'usegalaxy-eu.gxadmin'

### InfluxDB

[InfluxDB](https://www.influxdata.com/) provides the data storage for monitoring. It is a TSDB, so it has been designed specifically for storing time-series data like monitoring and metrics. TSBDs commonly feature some form of automatic data expiration after a set period of time. In InfluxDB these are known as “retention policies”. Outside of this feature, it is a relatively normal database.
This database store data transfered from galaxy by Telegraf.
InfluxDB is installed with the ansible role 'usegalaxy_eu.influxdb'

### Grafana

[Grafana](https://www.influxdata.com/) get the Galaxy metrics from the Influx database to create his graphics. InfluxDB is updated every 10s thanks to Telegraf, allowing Grafana to render live graphics. Grafana dashboards are accessible at "https://{{ galaxy_instance }}/grafana/"
Grafana is installed with the ansible role 'cloudalchemy.grafana' in playbook_galaxy-monitoring.yml

## uWSGI Zerg-mode

### transparent restart

The standard uWSGI operation mode allows you to restart the Galaxy application while blocking client connections. Zerg Mode does away with the waiting by running a special Zerg Pool process, and connecting zerg workers (aka Galaxy application processes) to the pool. As long as at least one is connected, requests can be served.

doc: https://uwsgi-docs.readthedocs.io/en/latest/Zerg.html

based on https://gist.github.com/natefoo/9dc5c349350770094c9fb14259e5c88a

To switch between zerg instances, uwsgi spinning fifo plugin is used.

### How it works

Instead of using uwsgi with mules to run Galaxy, we use uwsgi with zergpool, zergs and handlers:
 - zergpool: zerg server bind to a unix socket, configured in supervisor conf file zergpool.conf
 - zerg: instance attached to a zerg server (zergpool), handle web interface for galaxy, defined in supervisorctl config file zergs.conf and in the uwsgi section of galaxy.yml
 - handler: handle galaxy jobs to be run on cluster, works with a zerg running uwsgi

Supervisorctl group 'galaxy:' contains zergpool, zergs and handler

How to restart in zerg-mode, step by step:
1.  Start galaxy group 'galaxy:' with supervisorctl
    It will start zergpool, zergs and handlers. Zeerg0 and zerg1 will start running uwsgi at the same time, causing an error putting zerg0 in 'exited' state.
    That's ok, zerg1 will run uwsgi and galaxy will be accessible and functional.
2.  When galaxy is running, one zerg is in 'exited' state, let's say zerg0.
    To restart Galaxy transparantly, start that not running zerg with `supervisorctl start galaxy:zerg0`
    Zerg0 will start running a second instance of uwsgi, the first one being run by zerg1.
3.  Once the second instance of uwsgi is ready, it will kill the one that was running before.
    So Zerg0 will run a new instance of uwsgi stopping zerg1.
    Zerg1 is now in exited state and zerg0 is running uwsgi.
    This switch is transparant from user because Galaxy is always accessible from an uwsgi instance.
4.  To restart again, start the exited zerg with `supervisorctl start galaxy:zerg1`, now it is zerg1 in this example.