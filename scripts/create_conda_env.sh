#!/bin/bash

source $1   #conda.sh
conda create --yes --override-channels --channel conda-forge --channel defaults --name _galaxy_ python=3.7 pip>=9 virtualenv>=16 r-base=3.6.1
conda activate _galaxy_
virtualenv $2   #galaxy_source_dir/.venv
chown -R $3 $2
