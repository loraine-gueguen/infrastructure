import collections
import urllib.request
from yaml import dump, Dumper, safe_load, YAMLError

SLURM_CORES_STEP = 4
SLURM_CORES_MAX = 24

SLURM_MEM_DEFAULT = 2
SLURM_MEM_STEP = 8
SLURM_MEM_MAX = 128

def get_destination(tool, cores, mem, env, suffix, next_destination):
    destination_id = "slurm-%s-thread%02d-mem%03d" % (tool, cores, mem)

    destination = {
        'id': '%s%s' % (destination_id, suffix),
        'runner': 'slurm',
        'params': {
            'nativeSpecification': '--partition=fast  --cpus-per-task=%d --mem=%d' % (cores, mem * 1000)
        }
    }

    if len(env) >= 1:
        destination['env'] = env

    if (next_destination):
        destination['resubmit'] ={
                'condition': 'memory_limit_reached',
                'destination': next_destination
        }

    return(destination)

def main():

    print('Beginning file download with tool_destinations.yaml...')
    url = 'https://raw.githubusercontent.com/usegalaxy-eu/sorting-hat/master/tool_destinations.yaml'
    urllib.request.urlretrieve(url, "tool_destinations.yaml")

    with open("tool_destinations.yaml", 'r') as stream:
        try:
            tool_destinations  = safe_load(stream)
        except YAMLError as exc:
            print(exc)

    #with open('installed_tools.txt') as f:
    #    installed_tools = f.read().splitlines()

    print('Reading...')
    tools = {}
    destinations = {}
    for tool, value in tool_destinations.items():
        #if tool not in installed_tools:
            #print(tool)
            #continue

        cores = 1
        mem = SLURM_MEM_DEFAULT
        env = []
        if 'cores' in value.keys():
            cores = int(value['cores'])
        if 'mem' in value.keys():
            mem = int(value['mem'])
        if 'env'in value.keys():
            env = value['env']

        # To reduce the number of destinations
        #if 1 < cores:
        #    cores = int((((cores - 0.1) // SLURM_CORES_STEP) + 1) * SLURM_CORES_STEP)
        if SLURM_CORES_MAX <= cores:
            cores = SLURM_CORES_MAX

        #if SLURM_MEM_DEFAULT < mem:
        #    mem = int((((mem - 0.1) // SLURM_MEM_STEP) + 1) * SLURM_MEM_STEP)
        if SLURM_MEM_MAX <= mem:
            mem = SLURM_MEM_MAX

        destination_resubmit = get_destination(tool, cores, mem * 1.5, env, "_resubmit_mem", "")
        destination = get_destination(tool, cores, mem, env, '', destination_resubmit['id'])

        destinations[destination['id']] = destination
        destinations[destination_resubmit['id']] = destination_resubmit

        tools[tool] = {
            'id': tool,
            'destination': destination['id']
        }

    print('Writing...')

    plugins = []
    plugins.append({
        'id': 'local',
        'load': 'galaxy.jobs.runners.local:LocalJobRunner',
        'workers': '4'
    })
    plugins.append({
        'id': 'slurm',
        'load': 'galaxy.jobs.runners.slurm:SlurmJobRunner'
    })

    destination_resubmit = get_destination("default", 1, SLURM_MEM_DEFAULT * 1.5, [], "_resubmit_mem", "")
    destination = get_destination("default", 1, SLURM_MEM_DEFAULT, [], '', destination_resubmit['id'])
    destinations[destination['id']] = destination
    destinations[destination_resubmit['id']] = destination_resubmit

    default_destination =  destination['id']

    limits = {
        'registered_user_concurrent_jobs': '25',
        'anonymous_user_concurrent_jobs': '2'
    }

    destinations = collections.OrderedDict(sorted(destinations.items()))
    destinations = [ v for k, v in destinations.items() ]

    tools = collections.OrderedDict(sorted(tools.items()))
    tools = [ v for k, v in tools.items() ]

    outyaml = {
        'galaxy_jobconf': { 'plugins' : plugins, 'default_destination' : default_destination, 'destinations': destinations, 'tools': tools, 'limits': limits }
    }

    outfile = open("galaxy_jobconf.yml", 'w')
    Dumper.ignore_aliases = lambda *args : True
    dump(outyaml, outfile, default_flow_style=False)


if __name__ == "__main__":
    # execute only if run as a script
    main()
