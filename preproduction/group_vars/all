galaxy_root: /shared/mfs/data/galaxy

# galaxy database
galaxy_db_name: galaxy
galaxy_postgres_db_host: "{{ groups.galaxy_db | first }}"
galaxy_postgres_db_user: galaxy-admin
galaxy_postgres_db_pass: "{{ vault_galaxy_postgres_db_pass }}"
telegraf_postgres_db_user: telegraf
telegraf_postgres_db_pass: "{{ vault_telegraf_postgres_db_pass }}"

vault_galaxy_postgres_db_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          33306530626162353339336138303133656334636534613964326463393762663831313937373861
          3231653662313863393137326437353335623633383364340a626437343063316431323539613538
          61366565663332666361353934363262613663323934393036663534353862363139623439616365
          3063323464323363350a613432356135616465316335353136303963336536663737333063656137
          36613436643966396461636635313964636234363962396234353936333634663637

proftpd_sql_password: "{{ vault_galaxy_ftp_postgres_db_pass }}"

vault_galaxy_ftp_postgres_db_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          37656631336336383164386262303261373638666539323331623631343561653531306363326165
          3838366666623565656561383134653633633434303434320a653965303135356132376263626233
          30626566306238363663393861613833613736616331633336333737323537316262373833383438
          6463656333636664380a346263326336613639646133393435396232343235366162616566386536
          3866

vault_telegraf_postgres_db_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          39363732353531663732633562656235376166356633336133386530656163343465323335373033
          6634666630363434393365313032346634666334346435630a326330333630363937643437616632
          34653037303839613930663834336333336330376164333932376162663630366331643632653830
          3735393432323532390a313930653338373930366564303366306464386664636231343036666232
          6532

# galaxy ftp dir
galaxy_ftp_upload_dir: "{{ galaxy_root }}/ftp"
galaxy_server_dir: "{{ galaxy_root }}/server"

# nginx SSL Self-signed Certificate Configuration.
galaxy_frontend_create_self_signed_cert: "true"
galaxy_frontend_ssl_cert: "/etc/ssl/certs/galaxy_frontend_ssl.crt"
galaxy_frontend_ssl_csr: "/etc/ssl/certs/galaxy_frontend_ssl.csr"
galaxy_frontend_ssl_key: "/etc/ssl/private/galaxy_frontend_ssl.key"


galaxy_domain: "usegalaxy.ifb.local"
galaxy_welcome_root_url: "https://ifb-elixirfr.gitlab.io/usegalaxy-fr/welcome/"
galaxy_default_welcome: "{{ galaxy_welcome_root_url }}/base"

subdomains_dir: "/srv/subdomains"
galaxy_subdomains:
  - brand: ProteoRE
    subdomain: proteore
    welcome_url: "{{ galaxy_welcome_root_url }}/proteore/"
  - brand: Workflow4Metabolomics
    subdomain: workflow4metabolomics
    welcome_url: "{{ galaxy_welcome_root_url }}/workflow4metabolomics/"


# playbook_hpc_cvmfs
cvmfs_tool_data_local_path: "/shared/mfs/data/bank/data.galaxyproject.org/"
cvmfs_tool_data_local_cron: true

# Install the latest version
telegraf_agent_package_state: latest

# Configure the output to point to an InfluxDB
# place data in the "telegraf" database which will be created if need be.
telegraf_agent_output:
  - type: influxdb
    config:
    - urls = ["http://{{ groups.grafana | first }}:8086"]
    - database = "telegraf"

# The default plugins, applied to any telegraf-configured host
telegraf_plugins_default:
  - plugin: cpu
  - plugin: disk
  - plugin: kernel
  - plugin: processes
  - plugin: io
  - plugin: mem
  - plugin: system
  - plugin: swap
  - plugin: net
  - plugin: netstat

telegraf_plugins_extra:
  listen_galaxy_routes:
    plugin: "statsd"
    config:
      - service_address = ":8125"
      - metric_separator = "."
      - allowed_pending_messages = 10000
  monitor_galaxy_queue:
    plugin: "exec"
    config:
      - commands = ["/usr/bin/env PGDATABASE=galaxy PGHOST={{ groups['galaxy_db'][0] }} PGUSER={{ telegraf_postgres_db_user }} PGPASSWORD={{ telegraf_postgres_db_pass }} gxadmin iquery queue-overview --short-tool-id"]
      - timeout = "10s"
      - data_format = "influx"
      - interval = "15s"
  monitor_galaxy_tool-errors:
    plugin: "exec"
    config:
      - commands = ["/usr/bin/env PGDATABASE=galaxy PGHOST={{ groups['galaxy_db'][0] }} PGUSER={{ telegraf_postgres_db_user }} PGPASSWORD={{ telegraf_postgres_db_pass }} gxadmin iquery tool-errors --short-tool-id"]
      - timeout = "10s"
      - data_format = "influx"
      - interval = "15s"
