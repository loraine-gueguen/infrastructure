# job_conf_from_sorting_hat
This script convert the tool_destinations.yml file from [usegalaxy-eu/sorting-hat](https://github.com/usegalaxy-eu/sorting-hat/blob/master/tool_destinations.yaml) in a "classic" yaml format for the "classic" job_conf.xml

The result have to be copy/paste in "{{ inventory_dir }}/group_vars/galaxy/main.yml"

## Installation
```
conda create -y -n python3.7 python=3.7 pyyaml
```

## Usage
```
conda activate python3.7

git clone git@gitlab.com:ifb-elixirfr/usegalaxy-fr/tools.git /tmp/tools
cat /tmp/tools/files/*.yml | grep "^\- name:" | sed "s/- name: //" > installed_tools.txt

python job_conf_from_sorting_hat.py

sed -i "s|/data/2/galaxy_db/tmp|/shared/galaxy/tmp|" galaxy_jobconf.yml
```