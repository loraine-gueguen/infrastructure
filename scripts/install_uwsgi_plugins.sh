#!/bin/bash
#$1 galaxy_venv_dir
#install spinningfifo plugin for uwsgi

curl -LO https://gist.githubusercontent.com/unbit/2674313f070673a720e3/raw/56c804136c917ce1204b656f2d46e9988b48b1c7/spinningfifo.c	
mkdir -p $1/lib/uwsgi/plugins
$1/bin/uwsgi --build-plugin spinningfifo.c
mv spinningfifo_plugin.so $1/lib/uwsgi/plugins
rm spinningfifo.c
chown galaxy:galaxy $1/lib/uwsgi/plugins/spinningfifo_plugin.so
